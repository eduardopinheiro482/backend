Rails.application.routes.draw do
  post 'user_token' => 'user_token#create'

  resources :users do
    get :confirm_email
  end

  resources :comments

  resources :events do
    get '/comments', to: 'events#comments'
  end

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
