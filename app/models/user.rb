class User
  include Mongoid::Document
  include ActiveModel::SecurePassword

  before_create :confirmation_token

  field :name
  field :email
  field :email_confirmed, type: Boolean, default: false
  field :confirm_token
  field :password_digest

  has_secure_password

  def email_activate
    self.email_confirmed = true
    self.confirm_token = nil
    save!
  end

private
  def confirmation_token
    if self.confirm_token.blank?
      self.confirm_token = SecureRandom.urlsafe_base64.to_s
    end
  end
end
