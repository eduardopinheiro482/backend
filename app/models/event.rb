class Event
  include Mongoid::Document
  include Mongoid::Filterable

  field :id, type: Integer
  field :name, type: String
  field :description, type: String
  field :type, type: String
  field :url, type: String
  field :disponibility, type: String

  has_many :comments

  filter_by(:name)
  filter_by(:description)
  filter_by(:type)
end
