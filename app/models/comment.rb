class Comment
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  field :id, type: Integer
  field :name, type: String
  field :email, type: String
  field :comment, type: String
  belongs_to :event
end
