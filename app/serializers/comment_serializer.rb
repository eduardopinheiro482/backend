class CommentSerializer < ActiveModel::Serializer
  attributes :name, :email, :comment, :created_at

  belongs_to :event
end
