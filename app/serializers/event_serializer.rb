class EventSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :type, :url, :disponibility

  has_many :comments do
    link(:related) { event_comments_path(event_id: object.id) }
  end
end
