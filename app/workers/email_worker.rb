class EmailWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(id)
    UserMailer.welcome_email(id).deliver
  end
end