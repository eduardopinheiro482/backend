class UserTokenController < Knock::AuthTokenController
  skip_before_action :verify_authenticity_token

  def create
    @user = User.find(auth_token.payload[:sub])
    if @user.email_confirmed
      render json: {
        token: auth_token.token
      }, status: :created
    else
      render json: {"Message": "Please verify your email"}, status: 401
    end
  end

  rescue_from Knock.not_found_exception_class_name, with: :bad_request

  def bad_request
    render json: { error: "Invalid email address/password" }, status: :bad_request
  end

  private
    def auth_params
        params[:email] = params[:username] unless params[:email]

        params.permit(:email, :username, :password, user_token: [:username, :password])
    end
end
