class UsersController < ApplicationController
  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      EmailWorker.perform_async(@user.id)

      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def confirm_email
      user = User.find_by(confirm_token: params[:user_id])
      if user
        user.email_activate
        render json: user, status: :ok, location: @user
      else
        render json: "Error", status: :unprocessable_entity
      end
  end

  private
    # Only allow a trusted parameter "white list" through.
    def user_params
      ActiveModelSerializers::Deserialization.jsonapi_parse!(params, only: [:username, :email, :password])
    end
end
