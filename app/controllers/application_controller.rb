class ApplicationController < ActionController::API
  include Rails::Pagination
  include Knock::Authenticable
end
