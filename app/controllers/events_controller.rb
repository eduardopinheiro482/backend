class EventsController < ApplicationController
  before_action :set_event, only: [:show, :comments]
  before_action :authenticate_user,  only: [:create]

  # GET /events
  def index
    @events = paginate Event.filtrate(params[:filter])

    render json: @events
  end

  # GET /events/1
  def show
    render json: @event
  end

  # GET /events/1/comments
  def comments
    render json: @event.comments.order("created_at DESC")
  end

  # POST /events
  def create
    @event = Event.new(event_params)

    if @event.save
      render json: @event, status: :created, location: @event
    else
      render json: @event.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      params[:id] = params[:event_id] unless params[:id]
          
      @event = Event.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def event_params
      ActiveModelSerializers::Deserialization.jsonapi_parse!(params, only: [:name, :description, :type, :url, :disponibility])
    end
end
