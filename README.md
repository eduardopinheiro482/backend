# README

## Desafio Full stack (Quarantine.cult)

# Requirements

    ruby ~ 2.5.7
    rails ~ 5.2
    mongo ~ latest (currently using docker-compose to run it)

# Setup backend

    docker-compose up -d mongodb
    bundle install
    bundle exec rails s

# Run tests

    bundle exec rspec