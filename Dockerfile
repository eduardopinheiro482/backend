FROM ruby:2.5

WORKDIR /backend

ADD Gemfile /backend/

RUN bundle update
RUN bundle install

ADD . /backend/

RUN chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]
