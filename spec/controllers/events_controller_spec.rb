require 'rails_helper'

RSpec.describe EventsController, type: :controller do

  describe "GET #index" do
    before do
      (1..5).to_a.each do |i|
        Event.create(name: 'Event ' + i.to_s)
      end
      get :index, params: {
        page: {
          number: 1,
          size: 2
        }
      }, format: :json
    end

    after :each do
      Mongoid.purge!
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "response with JSON body containing events and pagination" do
      hash_body = nil
      expect { hash_body = JSON.parse(response.body).with_indifferent_access }.not_to raise_exception

      hash_body["data"].each do |event|
        expect(event["attributes"].keys).to match_array(["description", "disponibility", "name", "type", "url"])
      end

      expect(hash_body["data"][0]["attributes"]).to match({
        "description": nil,
        "disponibility": nil,
        "name": "Event 1",
        "type": nil,
        "url": nil,
      })

      expect(hash_body["links"].keys).to match_array(["self", "first", "last", "prev", "next"])
    end
  end


  describe "GET #show" do
    before do
      get :show, params: {id: event._id}, format: :json
    end

    after :each do
      Mongoid.purge!
    end

    let(:event) { Event.create(name: 'Event NoName') }

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "response with JSON body containing expected Events attributes" do
      hash_body = nil
      expect { hash_body = JSON.parse(response.body).with_indifferent_access }.not_to raise_exception
      expect(hash_body["data"]["attributes"].keys).to match_array(["description", "disponibility", "name", "type", "url"])
      expect(hash_body["data"]["attributes"]).to match({
        "description": nil,
        "disponibility": nil,
        "name": "Event NoName",
        "type": nil,
        "url": nil,
      })
    end
  end
end